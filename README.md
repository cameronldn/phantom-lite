# Phantom Lite

phantom-lite is a simple and responsive theme for [Ghost](http://github.com/tryghost/ghost/) based on the [Phantom](https://github.com/Bartinger/phantom) theme. 

It's compatible with ```Ghost (0.5.2)```.

Take a look at the live demo at [http://blog.cleyden.com](http://blog.cleyden.com)

## Configuration
*  Google Analytics 

Edit [partials/analytics.hbs](https://github.com/cleyden/phantom-lite/blob/master/partials/analytics.hbs) and set parameters to `ga('create', ...)` call.

	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-XXXXXXXXX-X', 'yoursite.com');
		ga('send', 'pageview');
	</script>

*  Disqus

Edit [partials/disqus.hbs](https://github.com/cleyden/phantom-lite/blob/master/partials/disqus.hbs) and set `disqus_shortname`.

	<div id="disqus_thread"></div>
	<script type="text/javascript">
		var disqus_shortname = '<username>';
		(function() {
			var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
			dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
			(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
		})();
	</script>
	<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>

*  Google Webmaster Tools

Edit [default.hbs](https://github.com/cleyden/phantom-lite/blob/master/default.hbss) and set your `content` value with a tag named `google-site-verification`.

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="google-site-verification" content="<your-site-verification>" />

	{{! Page Meta }}
	<title>{{meta_title}}</title>

*  Sidebar

Edit [partials/sidebar.hbs](https://github.com/cleyden/phantom-lite/blob/master/partials/sidebar.hbs) and fix the URLs and remove what's not needed.

	<div class="social-icons">
		<a href="https://github.com/<username>" class="icon icon-github"></a>
		<a href="https://plus.google.com/+<username>?rel=author" class="icon icon-gplus"></a>
		<a href="https://twitter.com/<username>" class="icon icon-twitter"></a>
		<a href="/rss" class="icon-rss"></a>
		<a href="mailto:<username>@email.com" class="icon-mail"></a>
	</div>

